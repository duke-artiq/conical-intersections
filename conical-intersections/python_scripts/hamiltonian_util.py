"""
Author: Jacob Whitlow
Date: 11/25/2020

Utility functions used in other classes for Hamiltonian evolution
"""

import numpy as np
from qutip import *


__all__ = ["evolution_by_steps", "get_expectations", "get_default_hamiltonian_one_ion", "get_cross_terms_one_ion",
           "get_jump_operators_one_ion", "get_default_hamiltonian_two_ions", "get_cross_terms_two_ions",
           "get_jump_operators_two_ions"]


def evolution_by_steps(times, Hamiltonians, psi0, jump_operators):
    """Perform step-by-step evolution based on list of Hamiltonians and times"""
    states = list()
    psi = psi0
    for h in range(len(Hamiltonians)):
        output = mesolve(Hamiltonians[h], psi, times[h], jump_operators)
        for state in output.states:
            states.append(state)
        psi = output.states[-1]
        del output
        print(h)
    return states


def get_expectations(states, operators):
    return [[expect(operator, state) for state in states] for operator in operators]


"""One ion"""


def get_default_hamiltonian_one_ion(N, epsilon_x, epsilon_y, rabi_x, rabi_y, delta_z):
    # Motional operators
    a = destroy(N)
    iden_N = identity(N)

    # Hamiltonians
    HO_X = 2 * np.pi * epsilon_x * tensor(tensor(a.dag() * a + (1 / 2) * identity(N), iden_N), identity(2))
    HO_Y = 2 * np.pi * epsilon_y * tensor(iden_N, tensor(a.dag() * a + (1 / 2) * identity(N)), identity(2))
    JT_X = np.pi * rabi_x * tensor(tensor(a + a.dag(), iden_N), sigmax())
    JT_Y = np.pi * rabi_y * tensor(tensor(iden_N, a + a.dag()), sigmay())
    H_Z = np.pi * delta_z * tensor(tensor(iden_N, iden_N), sigmaz())

    return [HO_X, HO_Y, JT_X, JT_Y, H_Z]


def get_cross_terms_one_ion(N, rabi_x_y, rabi_y_x):
    # Motional operators
    a = destroy(N)
    iden_N = identity(N)

    JT_Y_X_Position = np.pi * rabi_y_x * tensor(tensor(a + a.dag(), iden_N), sigmay())
    JT_X_Y_Position = np.pi * rabi_x_y * tensor(tensor(iden_N, a + a.dag()), sigmax())
    JT_Y_X_Momentum = np.pi * rabi_y_x * 1.j * tensor(tensor(a - a.dag(), iden_N), sigmay())
    JT_X_Y_Momentum = np.pi * rabi_x_y * 1.j * tensor(tensor(iden_N, a - a.dag()), sigmax())

    return [JT_Y_X_Position, JT_X_Y_Position, JT_Y_X_Momentum, JT_X_Y_Momentum]


def get_jump_operators_one_ion(N, sqrt_gamma_x, sqrt_gamma_y, sqrt_gamma_phase):
    # Motional operators
    a = destroy(N)
    iden_N = identity(N)

    L_gain_x = sqrt_gamma_x * tensor(tensor(a.dag(), iden_N), identity(2))
    L_lose_x = sqrt_gamma_x * tensor(tensor(a, iden_N), identity(2))
    L_phase_x = sqrt_gamma_phase * tensor(tensor(a.dag() * a, iden_N), identity(2))
    L_gain_y = sqrt_gamma_y * tensor(tensor(iden_N, a.dag()), identity(2))
    L_lose_y = sqrt_gamma_y * tensor(tensor(iden_N, a), identity(2))
    L_phase_y = sqrt_gamma_phase * tensor(tensor(iden_N, a.dag() * a), identity(2))

    return [L_gain_x, L_lose_x, L_phase_x, L_gain_y, L_lose_y, L_phase_y]


"""Two ions"""


def get_default_hamiltonian_two_ions(N_t, N_cm, epsilon_xt, epsilon_yt, rabi_xt,
                                     rabi_yt, delta_xt_xc, delta_yt_yc, delta_z):
    # Motional operators
    a_t = destroy(N_t)
    iden_Nt = identity(N_t)
    a_cm = destroy(N_cm)
    iden_Ncm = identity(N_cm)
    N_xt = tensor(tensor(tensor(a_t.dag() * a_t, iden_Nt), tensor(iden_Ncm, iden_Ncm)), identity(2))
    N_yt = tensor(tensor(tensor(iden_Nt, a_t.dag() * a_t), tensor(iden_Ncm, iden_Ncm)), identity(2))
    N_xc = tensor(tensor(tensor(iden_Nt, iden_Nt), tensor(a_cm.dag() * a_cm, iden_Ncm)), identity(2))
    N_yc = tensor(tensor(tensor(iden_Nt, iden_Nt), tensor(iden_Ncm, a_cm.dag() * a_cm)), identity(2))

    # Hamiltonians
    HO_XT = 2 * np.pi * epsilon_xt * N_xt
    HO_YT = 2 * np.pi * epsilon_yt * N_yt
    HO_XC = 2 * np.pi * delta_xt_xc * N_xc
    HO_YC = 2 * np.pi * delta_yt_yc * N_yc
    JT_XT = np.pi * rabi_xt * tensor(tensor(tensor(a_t + a_t.dag(), iden_Nt), tensor(iden_Ncm, iden_Ncm)), sigmax())
    JT_YT = np.pi * rabi_yt * tensor(tensor(tensor(iden_Nt, a_t + a_t.dag()), tensor(iden_Ncm, iden_Ncm)), sigmay())
    H_Z = np.pi * delta_z * tensor(tensor(tensor(iden_Nt, iden_Nt), tensor(iden_Ncm, iden_Ncm)), sigmaz())

    return [HO_XT, HO_YT, HO_XC, HO_YC, JT_XT, JT_YT, H_Z]


def get_cross_terms_two_ions(N_t, N_cm, rabi_xc, rabi_yc):
    # Motional operators
    a_t = destroy(N_t)
    a_cm = destroy(N_cm)
    iden_Nt = identity(N_t)
    iden_Ncm = identity(N_cm)

    # Cross terms
    JT_XT_XC = np.pi * rabi_xc * tensor(tensor(iden_Nt, iden_Nt), tensor(tensor(a_cm + a_cm.dag(), iden_Ncm)), sigmax())
    JT_YT_YC = np.pi * rabi_yc * tensor(tensor(tensor(iden_Nt, iden_Nt), tensor(iden_Ncm, a_cm + a_cm.dag())), sigmay())

    return [JT_XT_XC, JT_YT_YC]


def get_jump_operators_two_ions(N_t, N_cm, sqrt_gamma_xt, sqrt_gamma_yt,
                                sqrt_gamma_xc, sqrt_gamma_yc, sqrt_gamma_phase):
    # Motional operators
    a_t = destroy(N_t)
    iden_Nt = identity(N_t)
    a_cm = destroy(N_cm)
    iden_Ncm = identity(N_cm)

    L_gain_xt = sqrt_gamma_xt * tensor(tensor(tensor(a_t.dag(), iden_Nt), tensor(iden_Ncm, iden_Ncm)), identity(2))
    L_lose_xt = sqrt_gamma_xt * tensor(tensor(tensor(a_t, iden_Nt), tensor(iden_Ncm, iden_Ncm)), identity(2))
    L_phase_xt = sqrt_gamma_phase * tensor(tensor(tensor(a_t.dag() * a_t, iden_Nt), tensor(iden_Ncm, iden_Ncm)),
                                           identity(2))
    L_gain_yt = sqrt_gamma_yt * tensor(tensor(tensor(iden_Nt, a_t.dag()), tensor(iden_Ncm, iden_Ncm)), identity(2))
    L_lose_yt = sqrt_gamma_yt * tensor(tensor(tensor(iden_Nt, a_t), tensor(iden_Ncm, iden_Ncm)), identity(2))
    L_phase_yt = sqrt_gamma_phase * tensor(tensor(tensor(iden_Nt, a_t.dag() * a_t), tensor(iden_Ncm, iden_Ncm)),
                                           identity(2))

    L_gain_xc = sqrt_gamma_xc * tensor(tensor(tensor(iden_Nt, iden_Nt), tensor(a_cm.dag(), iden_Ncm)), identity(2))
    L_lose_xc = sqrt_gamma_xc * tensor(tensor(tensor(iden_Nt, iden_Nt), tensor(a_cm, iden_Ncm)), identity(2))
    L_phase_xc = sqrt_gamma_phase * tensor(tensor(tensor(iden_Nt, iden_Nt), tensor(a_cm.dag() * a_cm, iden_Ncm)),
                                           identity(2))
    L_gain_yc = sqrt_gamma_yc * tensor(tensor(tensor(iden_Nt, iden_Nt), tensor(iden_Ncm, a_cm.dag())), identity(2))
    L_lose_yc = sqrt_gamma_yc * tensor(tensor(tensor(iden_Nt, iden_Nt), tensor(iden_Ncm, a_cm)), identity(2))
    L_phase_yc = sqrt_gamma_phase * tensor(tensor(tensor(iden_Nt, iden_Nt), tensor(iden_Ncm, a_cm.dag() * a_cm)),
                                           identity(2))

    return [L_gain_xt, L_lose_xt, L_phase_xt, L_gain_yt, L_lose_yt, L_phase_yt,
            L_gain_xc, L_lose_xc, L_phase_xc, L_gain_yc, L_lose_yc, L_phase_yc]
