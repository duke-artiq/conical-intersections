"""
Author: Jacob Whitlow
Date: 11/25/2020

Utility functions used in other classes for plotting spatial distribution
"""

import numpy as np
from math import factorial


__all__ = ["make_hr", "get_psi", "get_2d_spatial_distribution"]


def make_hr(N):
    """Return a list of np.poly1d objects representing Hermite polynomials."""

    # Define the Hermite polynomials up to order VMAX by recursion:
    # H_[v] = 2qH_[v-1] - 2(v-1)H_[v-2]
    Hr = [None] * (N + 1)
    Hr[0] = np.poly1d([1.,])
    Hr[1] = np.poly1d([2., 0.])
    for v in range(2, N+1):
        Hr[v] = Hr[1]*Hr[v-1] - 2*(v-1)*Hr[v-2]
    return Hr


def get_psi(v, q, Hr):
    """Return the harmonic oscillator wavefunction for level v on grid q."""
    normalize = lambda v: 1. / np.sqrt(np.sqrt(np.pi) * 2 ** (v - 1) * factorial(v))
    return normalize(v)*Hr[v](q)*np.exp(-q*q/2.)


def get_2d_spatial_distribution(rho, N, xvec):
    """Get 2D spatial distribution from motional modes"""
    Hr = make_hr(N)

    overall_probs = rho[0][0][0]*np.outer(get_psi(0, xvec, Hr),get_psi(0, xvec, Hr))*\
                            np.conj(np.outer(get_psi(0, xvec, Hr),get_psi(0, xvec, Hr)))

    for i in range(N):
        for j in range(N):
            for k in range(N):
                for l in range(N):
                    probs = rho[i*N+j][0][k*N+l]*np.outer(get_psi(j, xvec, Hr),get_psi(i, xvec, Hr))*\
                            np.conj(np.outer(get_psi(l, xvec, Hr),get_psi(k, xvec, Hr)))
                    if i == 0 and j == 0 and k == 0 and l == 0:
                        overall_probs = probs
                    else:
                        overall_probs += probs
    return overall_probs
