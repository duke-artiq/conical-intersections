"""
Author: Jacob Whitlow
Date: 11/25/2020

Utility functions used in other classes for plotting Wigner distribution
"""

import numpy as np
from qutip import *
from scipy.optimize import curve_fit


__all__ = ["get_1_ion_probe_x", "get_1_ion_probe_y", "get_2_ion_probe_x", "get_2_ion_probe_y", "wigner_via_curve_fit",
           "four_dimensional_wigner_via_parity", "four_dimensional_wigner_via_probe"]


def get_1_ion_probe_x(beta, rabi_freq, N):
    iden_N = qeye(N)
    a = destroy(N)
    orig_probe = rabi_freq * tensor(tensor(a, iden_N), sigmap()) + rabi_freq * tensor(tensor(a.dag(), iden_N), sigmam())
    new_probe = rabi_freq * tensor(np.conj(beta) * tensor(iden_N, iden_N), sigmap()) + rabi_freq * tensor(
        beta * tensor(iden_N, iden_N),
        sigmam())
    return orig_probe + new_probe


def get_1_ion_probe_y(beta, rabi_freq, N):
    iden_N = qeye(N)
    a = destroy(N)
    orig_probe = rabi_freq * tensor(tensor(iden_N, a), sigmap()) + rabi_freq * tensor(tensor(iden_N, a.dag()), sigmam())
    new_probe = rabi_freq * tensor(np.conj(beta) * tensor(iden_N, iden_N), sigmap()) + rabi_freq * tensor(
        beta * tensor(iden_N, iden_N),
        sigmam())
    return orig_probe + new_probe


def get_2_ion_probe_x(beta, rabi_freq, N):
    iden_N = qeye(N)
    a = destroy(N)
    orig_probe = \
        rabi_freq * tensor(tensor(tensor(a, iden_N), identity(2)), sigmap()) + \
        rabi_freq * tensor(tensor(tensor(a.dag(), iden_N), identity(2)), sigmam())
    new_probe = \
        rabi_freq * np.conj(beta) * tensor(tensor(tensor(iden_N, iden_N), identity(2)), sigmap()) + \
        rabi_freq * beta * tensor(tensor(tensor(iden_N, iden_N), identity(2)), sigmam())
    return orig_probe + new_probe


def get_2_ion_probe_y(beta, rabi_freq, N):
    iden_N = qeye(N)
    a = destroy(N)
    orig_probe = tensor(
        rabi_freq * tensor(tensor(iden_N, a), sigmap()) +
        rabi_freq * tensor(tensor(iden_N, a.dag()), sigmam()),
        identity(2))
    new_probe = tensor(
        rabi_freq * np.conj(beta) * tensor(tensor(iden_N, iden_N), sigmap()) +
        rabi_freq * beta * tensor(tensor(iden_N, iden_N), sigmam()),
        identity(2))
    return orig_probe + new_probe


def wigner_via_curve_fit(psi0, N, rabi, real_spacing, imag_spacing, t_probe, gamma=0, x_or_y="x", jump_operators=None):

    iden_N = qeye(N)
    project_up = tensor(tensor(iden_N, iden_N), basis(2, 0) * basis(2, 0).dag())
    prob_down = (1 - tensor(tensor(identity(N), identity(N)), sigmaz())) / 2

    Mn = list()
    for n in range(N):
        Mn.append((basis(N + 1, n + 1).dag() * (destroy(N + 1) + create(N + 1)) * basis(N + 1, n))[0][0].real)

    def probe_prob_of_down(t, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9):
        mot_probs = [p0, p1, p2, p3, p4, p5, p6, p7, p8, p9]
        probs = (1 / 2) * mot_probs[0] * (1 - np.exp(-gamma * t) * np.cos(2*np.pi*rabi * Mn[0] * t))
        for n in range(1, N):
            probs += (1 / 2) * mot_probs[n] * (1 - np.exp(-gamma * t) * np.cos(2*np.pi*rabi * Mn[n] * t))
        return probs

    k = 0
    wigner = list()
    for beta_real in real_spacing:
        wigner.append(list())
        for beta_im in imag_spacing:
            # Probe
            beta_val = beta_real + 1.j * beta_im
            if x_or_y == "x":
                probe = get_1_ion_probe_x(beta_val, 2*np.pi*rabi, N)
            else:
                probe = get_1_ion_probe_y(beta_val, 2*np.pi*rabi, N)

            psi = project_up * psi0 * project_up
            psi = psi / psi.norm()
            output = mesolve(probe, psi, t_probe, jump_operators)

            probe_data = output.states
            prob_down_exp = list()
            for state in probe_data:
                prob_down_exp.append(expect(prob_down, state))
            popt, pcov = curve_fit(probe_prob_of_down, t_probe, prob_down_exp, bounds=[0, 1])
            cur_val = 0
            for n in range(len(popt)):
                cur_val += ((-1) ** n) * popt[n]
            wigner[k].append((2 / np.pi) * cur_val)
            del probe_data
            del output
        k += 1

    return wigner


def four_dimensional_wigner_via_parity(psi0, N, alpha_real_spacing, alpha_imag_spacing,
                                       beta_real_spacing, beta_imag_spacing):
    # Motional operators
    a = destroy(N)
    iden_N = identity(N)

    parity_x = tensor(tensor((1.j*np.pi*a.dag()*a).expm(), iden_N), identity(2))
    parity_y = tensor(tensor(iden_N, (1.j*np.pi*a.dag()*a).expm()), identity(2))

    k = 0
    wigner = list()
    for alpha_real in alpha_real_spacing:
        l = 0
        wigner.append(list())
        for alpha_im in alpha_imag_spacing:
            q = 0
            wigner[k].append(list())
            for beta_real in beta_real_spacing:
                r = 0
                wigner[k][l].append(list())
                for beta_im in beta_imag_spacing:
                    # Probe
                    alpha_val = alpha_real + 1.j * alpha_im
                    beta_val = beta_real + 1.j * beta_im
                    displacement_x = tensor(tensor(displace(N, alpha_val), iden_N), identity(2))
                    displacement_y = tensor(tensor(iden_N, displace(N, beta_val)), identity(2))

                    diff = expect(displacement_x * parity_x * displacement_x.dag() *
                                  displacement_y * parity_y * displacement_y.dag(),
                                  psi0)

                    wigner[k][l][q].append((4 / (np.pi ** 2)) * diff)

                    print(k)
                    print(l)
                    print(q)
                    print(r)
                    print()
                    r += 1
                q += 1
            l += 1
        k += 1

    return wigner


def four_dimensional_wigner_via_probe(psi0, N, rabi, eta, nu_x, nu_y, alpha_real_spacing, alpha_imag_spacing,
                                      beta_real_spacing, beta_imag_spacing):
    # Motional operators
    num_op = num(N)
    a = destroy(N)
    iden_N = identity(N)

    up_minus_down = tensor(tensor(iden_N, iden_N), sigmaz())
    project_down = tensor(tensor(iden_N, iden_N), basis(2, 1) * basis(2, 1).dag())
    project_up = tensor(tensor(iden_N, iden_N), basis(2, 0) * basis(2, 0).dag())

    tau = 1 / (2* rabi * eta ** 2)
    phi = (2*np.pi*rabi * tau - np.pi) / 2
    cos_2_phi = np.cos(2 * phi)

    # Uses two lasers (could potentially use one, would make easier setup?)
    H_x = 2*np.pi*nu_x * tensor(tensor(num_op, iden_N), identity(2)) + (np.pi*rabi) * tensor(
        (1.j * tensor(eta * (a + a.dag()), iden_N)).expm(), sigmap()) + (np.pi*rabi) * tensor(
        (-1.j * tensor(eta * (a + a.dag()), iden_N)).expm(), sigmam())

    H_y = 2*np.pi*nu_y * tensor(tensor(iden_N, num_op), identity(2)) + (np.pi*rabi) * tensor(
        (1.j * tensor(iden_N, eta * (a + a.dag()))).expm(), sigmap()) + (np.pi*rabi) * tensor(
        (-1.j * tensor(iden_N, eta * (a + a.dag()))).expm(), sigmam())

    probe_x = (-1.j * H_x * tau).expm()
    probe_y = (-1.j * H_y * tau).expm()

    k = 0
    wigner = list()
    psi_down = (project_down * psi0 * project_down).unit()
    psi_up = (project_up * psi0 * project_up).unit()
    for alpha_real in alpha_real_spacing:
        l = 0
        wigner.append(list())
        for alpha_im in alpha_imag_spacing:
            alpha_val = -alpha_real - 1.j * alpha_im
            displacement_x = tensor(tensor(displace(N, alpha_val), iden_N), identity(2))

            psi_down_x = displacement_x * psi_down * displacement_x.dag()
            psi_down_x = probe_x * psi_down_x * probe_x.dag()

            psi_up_x = displacement_x * psi_up * displacement_x.dag()
            psi_up_x = probe_x * psi_up_x * probe_x.dag()

            q = 0
            wigner[k].append(list())
            for beta_real in beta_real_spacing:
                r = 0
                wigner[k][l].append(list())
                for beta_im in beta_imag_spacing:
                    # Probe
                    beta_val = -beta_real - 1.j * beta_im
                    displacement_y = tensor(tensor(iden_N, displace(N, beta_val)), identity(2))

                    psi_down_xy = displacement_y * psi_down_x * displacement_y.dag()
                    psi_down_xy = probe_y * psi_down_xy * probe_y.dag()
                    diff_down = (-1) * expect(up_minus_down, psi_down_xy)

                    psi_up_xy = displacement_y * psi_up_x * displacement_y.dag()
                    psi_up_xy = probe_y * psi_up_xy * probe_y.dag()
                    diff_up = expect(up_minus_down, psi_up_xy)

                    wigner[k][l][q].append(2 * (diff_up + diff_down) / cos_2_phi)

                    print(k)
                    print(l)
                    print(q)
                    print(r)
                    print()
                    r += 1
                q += 1
            l += 1
        k += 1

    return wigner