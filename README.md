# Conical Intersections

## Description
This project is designed to accompany the following paper: https://arxiv.org/pdf/2211.07319.pdf

The notebooks in the conical-intersections folder should reproduce all of the plots.  They utilize the raw data in the data folder.

## Support
Plese contact Jacob Whitlow at jacob.whitlow@duke.edu for support on this project.

## Authors
Primary Author: Jacob Whitlow (jacob.whitlow@duke.edu)
Supporting Author: Zhubing Jia (zhubing.jia@duke.edu)

## License
Apache 2.0

## Acknowledgements
This work was supported by the Office of the Director of National Intelligence, Intelligence Advanced Research Projects Activity through ARO Contract W911NF-16-1-0082, the National Science Foundation STAQ Project Phy-181891, and the U.S. Department of Energy, Office of Advanced Scientific Computing Research QSCOUT program,  DOE Basic Energy Sciences Award No. DE-0019449, ARO MURI Grant No. W911NF-18-1-0218, and the NSF Quantum Leap Challenge  Institute for Robust Quantum Simulation Grant No. OMA-2120757.
